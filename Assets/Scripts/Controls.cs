﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    public GameObject cube;

    public int tries;
    public Terrain terrain;
    Transform t;
    public float speed = 0.1f;
    public int size = 1025;
    // Start is called before the first frame update
    void Start()
    {
        Random random = new Random();
        t = (Transform)cube.GetComponent("Transform");
        float[,] heights = new float[size, size];
        float max = -1;
        float min = 2;
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
            {
                heights[i, j] =
                Mathf.PerlinNoise(i * 1f / size, j * 1f / size) +
                0.5f * Mathf.PerlinNoise(i * 2f / size, j * 2f / size) +
                0.25f * Mathf.PerlinNoise(i * 4f / size, j * 4f / size);
                max = Mathf.Max(max, heights[i, j]);
                min = Mathf.Min(min, heights[i, j]);
            }
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                heights[i, j] = clamp(heights[i, j], min, max);
        terrain.terrainData.SetHeights(0, 0, heights);

        Vector2[] turns = new Vector2[]{
                new Vector2(-1,-1),
                new Vector2(-1,0),
                new Vector2(-1,1),
                new Vector2(0,1),
                new Vector2(1,1),
                new Vector2(1,0),
                new Vector2(1,-1),
                new Vector2(-1,-1)
            };

        for (int a = 0; a < tries; a++)
        {
            int x = Mathf.RoundToInt(Random.Range(0, size));
            int y = Mathf.RoundToInt(Random.Range(0, size));
            int counter = 0;


            while (true)
            {
                counter++;
                float newcenter = heights[x, y];
                float oldcenter = newcenter;
                int nx = -1, ny = -1;
                for (int i = x - 1; i <= x + 1; i++)
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (i < 0 || i >= size || j < 0 || j >= size)
                            continue;
                        if (heights[i, j] < newcenter)
                        {
                            newcenter = heights[i, j];
                            nx = i;
                            ny = j;
                        }
                    }
                if (nx == -1 || ny == -1 || counter > 1000)
                    break;

                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.localScale = new Vector3(2, 2, 2);
                sphere.transform.position = new Vector3(ny, heights[nx, ny] * 300, nx);
                x = nx;
                y = ny;

            }
        }

    }

    float clamp(float v, float min, float max)
    {
        return (v - min) * (1 / (max - min));
    }

    // Update is called once per frame
    void Update()
    {
        var pos = t.position;
        if (Input.GetKey("w"))
            pos.x += speed;
        if (Input.GetKey("s"))
            pos.x -= speed;
        if (Input.GetKey("a"))
            pos.z += speed;
        if (Input.GetKey("d"))
            pos.z -= speed;
        t.SetPositionAndRotation(pos, t.rotation);

    }
}
